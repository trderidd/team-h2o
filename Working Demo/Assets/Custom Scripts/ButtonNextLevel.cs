﻿using UnityEngine;
using System.Collections;

public class ButtonNextLevel : MonoBehaviour 
{
	void Start(){
		Time.timeScale=1;
	}
	public void NextLevelButton(int index)
	{
		Application.LoadLevel(index);
	}
	
	public void NextLevelButton(string levelName)
	{
		Application.LoadLevel(levelName);
	}
	public void ResetLevel(){
		Application.LoadLevel (Application.loadedLevel);
	}
}