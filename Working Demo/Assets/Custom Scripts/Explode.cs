﻿using UnityEngine;
using System.Collections;


public class Explode : MonoBehaviour {

	public float explosion_delay;
	public float target_delay = 3f;
	public float explosion_rate = .01f;
	public float explosion_max_size = 10f;
	public float explosion_speed = 1f;
	public float current_radius = 0f;
	public float force = 10f;
	public GameObject bombCount;
	bool exploded = false;
	bool lit = false;
	CircleCollider2D explosion_radius;
	// Use this for initialization
	void Start () {
		bombCount = GameObject.FindGameObjectWithTag("SpawnCounter");
		explosion_radius = gameObject.GetComponent<CircleCollider2D>();
		explosion_delay = target_delay;
	}
	
	// Update is called once per frame
	void Update () {
		if(this.gameObject.transform.parent.GetComponent<Moveable2D>().pause == true){
			explosion_delay = target_delay;
		}
		else if(lit){
			explosion_delay -= Time.deltaTime;
			if(explosion_delay < 0)
			{
				exploded = true;
			}
		}
	}
	void FixedUpdate(){
		if(exploded){
			if(current_radius < explosion_max_size){
				current_radius += explosion_rate;
			}
			else
			{
				bombCount.GetComponent<RespawnCount>().refreshBombs();
				Object.Destroy(this.gameObject.transform.parent.gameObject);
			}
			explosion_radius.radius = current_radius;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == "DynamicParticle"){
			lit = true;
		}
		if(exploded){
			if(other.gameObject.tag == "DynamicParticle"){
				Vector2 target = other.gameObject.transform.position;
				Vector2 bomb = this.gameObject.transform.position;

				Vector2 direction = 800f * force * (target - bomb);
				other.gameObject.GetComponent<Rigidbody2D>().AddForce(direction);
			}
		}
	}
}
