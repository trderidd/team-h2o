﻿using UnityEngine;
using System.Collections;

public class Moveable2D : MonoBehaviour {
	
	public SpringJoint2D spring;
	public GameObject newPrefab;
	int prefabType;
	Vector2 startPosition;
	public bool pause = true;
	public GameObject spawnCounter;
	public GameObject pauseButton;

	
	void Start(){
		spawnCounter = GameObject.FindGameObjectWithTag("SpawnCounter");
		newPrefab = gameObject;
	}
	void Awake()
	{
		if(pause){
			spring = this.gameObject.GetComponent<SpringJoint2D>(); //"spring" is the SpringJoint2D component that I added to my object
			spring.connectedAnchor = gameObject.transform.position;//i want the anchor position to start at the object's position
			startPosition = transform.position;
			if (this.gameObject.tag == "Evaporator")
				prefabType = 1;
			else if (this.gameObject.tag == "Condensor")
				prefabType = 2;
			else if(this.gameObject.tag == "Frozen")
				prefabType = 3;
			else if(this.gameObject.tag == "Explosive")
				prefabType = 4;
			else
				prefabType = 5;
		}

	}
	
	
	void OnMouseDown()
	{
		if(pause){
			spring.enabled = true;//I'm reactivating the SpringJoint2D component each time I'm clicking on my object becouse I'm disabling it after I'm releasing the mouse click so it will fly in the direction i was moving my mouse
			Respawn ();
		}
	}
	
	
	void OnMouseDrag()        
	{
		if(pause){
			if (spring.enabled == true) 
			{
			
				Vector2 cursorPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);//getting cursor position
			
				spring.connectedAnchor = cursorPosition;//the anchor get's cursor's position
			}
			
		}
	}
	
	
	void OnMouseUp()        
	{
		if(pause){
			spring.enabled = false;//disabling the spring component

		}
	}

	void Respawn () {
		if (DecrementCount (prefabType)) {
			Instantiate (newPrefab, startPosition, Quaternion.identity); //Respawn prefab
		}
	}

	/*decrement the appopriate static counter*/

	bool DecrementCount (int counter) {

		if (counter == 1) {
			spawnCounter.GetComponent<RespawnCount>().evaporatorSpawn();
			if (spawnCounter.GetComponent<RespawnCount>().evaporator <= 0)
				return false;
		} 
		else if (counter == 2) {
			spawnCounter.GetComponent<RespawnCount>().condensorSpawn();
			if (spawnCounter.GetComponent<RespawnCount>().condensor <= 0)
				return false;
		} 
		else if (counter == 3) {
			spawnCounter.GetComponent<RespawnCount>().freezerSpawn();
			if (spawnCounter.GetComponent<RespawnCount>().freezer <= 0)
				return false;
		} 

		else if (counter == 4) {
			spawnCounter.GetComponent<RespawnCount>().explosiveSpawn();
			if (spawnCounter.GetComponent<RespawnCount>().explosive <= 0)
				return false;
		} 
		return true;
	}

	public void PauseButton(){
		pause = !pause;
	}
}