﻿using UnityEngine;
using System;


public class DeleteBox : MonoBehaviour
{
	public GameObject respawn;
	void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "DynamicParticle(Clone)")
        {
			respawn = GameObject.FindGameObjectWithTag("ParticleSource");
			respawn.GetComponent<ParticleGenerator>().AddCount();
            Destroy(col.gameObject);

        }
     }

}
