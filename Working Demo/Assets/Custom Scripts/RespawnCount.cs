﻿using UnityEngine;
using System.Collections;

public class RespawnCount : MonoBehaviour {
	public int evaporator, condensor, freezer, explosive, startExplosive, explodedNumber =0;
	public GameObject firstBomb;
	GameObject newPrefab;
	GameObject generator;
	SpringJoint2D spring;
	Vector2 startPosition;
	bool bombRestart = false;
	bool subRestart = false;
	// Use this for initialization
	void Start () {
		newPrefab = firstBomb;
		startPosition = firstBomb.transform.position;
		generator =  GameObject.FindGameObjectWithTag("ParticleSource");
	}
	
	// Update is called once per frame
	void Update () {
		if(explodedNumber == startExplosive && generator.gameObject.GetComponent<ParticleGenerator>().pause){
			refreshBombs();
		}
		else if (bombRestart && generator.gameObject.GetComponent<ParticleGenerator>().pause){
			explosive = startExplosive;
			explodedNumber = 0;
			bombRestart = false;
		}
	}

	public void evaporatorSpawn(){
		evaporator--;
	}
	public void condensorSpawn(){
		condensor--;
	}
	public void freezerSpawn(){
		freezer--;
	}
	public void explosiveSpawn(){
		explosive--;
	}
	public void refreshBombs(){
		if(explodedNumber == startExplosive && generator.gameObject.GetComponent<ParticleGenerator>().pause){
			Instantiate (newPrefab, startPosition, Quaternion.identity); //Respawn prefab
			explosive=startExplosive;
			explodedNumber = 0;
		}
		else{
			bombRestart = true;
			explodedNumber++;
		}
	}


}
