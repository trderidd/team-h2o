﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	public GameObject prefabElement;
	public int counter;

	// Use this for initialization
	void Start () {
		counter = 5;
		Spawn ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Spawn () {
		print (counter);
		GameObject element;
		if (counter > 0) {
			element = Instantiate (prefabElement, new Vector2 (-10, 5), Quaternion.identity) as GameObject;
			counter--;
		} else
			print ("Empty");
	}
}
