﻿using UnityEngine;
using System.Collections;

public class LimitedUse : MonoBehaviour {
	public int usesCount = 100;
	void OnCollisionEnter2D(Collision2D other){
		if ( other.gameObject.tag == "DynamicParticle") {
			usesCount = 100;
		if(usesCount <=0)
				Destroy(gameObject);
		}
	}
	
}