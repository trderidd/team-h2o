﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public Canvas pauseCanvas;
	bool pause = true;
	// Use this for initialization
	void Start () 
	{
		pauseCanvas = GetComponent<Canvas>();
		pauseCanvas.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown("escape")) //Pause game when escape is pressed
		{
			pause = !pause;
		}
		if(!pause){
			Time.timeScale = 0;
			pauseCanvas.enabled = true;
		}
		if(pause){
			Time.timeScale = 1;
			pauseCanvas.enabled = false;
		}

	}
}