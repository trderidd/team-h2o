﻿using UnityEngine;
using System;

public class CountBox : MonoBehaviour
{

	public bool paused;
    int particlesFinished = 0;
    void OnTriggerEnter2D(Collider2D col)
    {

        Debug.Log("entered box");
		if(col.GetComponent<DynamicParticle>().currentState == DynamicParticle.STATES.WATER){
			particlesFinished++;
		}

        if (particlesFinished >= 100)
        {
            Debug.Log("completion");//level complete message
			try{
            Application.LoadLevel(Application.loadedLevel + 1); //can use index later when we have more levels
			}
			catch{
				Application.LoadLevel(0);
			}
        }
    }

    void OnTriggerExit2D(Collider2D thing)
    {
		if(thing.GetComponent<DynamicParticle>().currentState == DynamicParticle.STATES.WATER){
			particlesFinished--;
		}

    }
	private GUIStyle guiStyle = new GUIStyle();
	void OnGUI()
	{
		guiStyle.fontSize =50;
		if(particlesFinished < 100){
			GUILayout.Label(particlesFinished.ToString() + "/100% Complete", guiStyle);
		}
		else{
			GUILayout.Label("100/100% Complete", guiStyle);
		}
	}
	public void Pause(){
		paused = !paused;
		particlesFinished = 0;
	}


}
