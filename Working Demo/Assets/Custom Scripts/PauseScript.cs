﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {
	public GameObject[] respawns;
	// Use this for initialization
	void Start () {

	}			
	// Update is called once per frame
	void Update () {	
	}

	public void Pause () {

		//finds all objects with the tags {Condensor, Evaporator, amd Frozen} and pauses them so they aren't moveable
		respawns = GameObject.FindGameObjectsWithTag("Condensor");
		foreach(GameObject respawn in respawns){
			respawn.GetComponent<Moveable2D>().PauseButton();
		}
		respawns = GameObject.FindGameObjectsWithTag("Evaporator");
		foreach(GameObject respawn in respawns){
			respawn.GetComponent<Moveable2D>().PauseButton();
		}	
		respawns = GameObject.FindGameObjectsWithTag("Frozen");
		foreach(GameObject respawn in respawns){
			respawn.GetComponent<Moveable2D>().PauseButton();
		}	
		respawns = GameObject.FindGameObjectsWithTag("Explosive");
		foreach(GameObject respawn in respawns){
			respawn.GetComponent<Moveable2D>().PauseButton();
		}
	}
}
